from similarity.jarowinkler import JaroWinkler
from similarity.jaccard import Jaccard
from similarity.metric_lcs import MetricLCS
from stop_words import get_stop_words
from nltk.corpus import stopwords
import string
import Levenshtein
import numpy as np

del_dict = {sp_char : " " for sp_char in string.punctuation} # a dict to indicate meaningless characters
table = str.maketrans(del_dict) # a translate table

class StringProcess():
    '''Used to pre-process string'''    
    def __init__(self):
        '''Nothing to do with this, just initialize an instance'''
        pass
    
    def string_vectorizer(self, s1, s2):
        '''Using Levenshtein to vectorize a pair of string in term of similarity'''

        uniqueNumberCount = Levenshtein.get_unique_number_count(s1, s2)
        numberMatchRate = Levenshtein.get_rate(s1, s2)
        matchScore = Levenshtein.sorted_levenshtein_rate(s1, s2)
        normalizedMatchRate = np.log((numberMatchRate + 2)) 
        jaro = Levenshtein.jarowinkler(s1, s2)
        jaccard = Levenshtein.jarowinkler(s1, s2)
        metric = Levenshtein.metric_lcs(s1, s2)

        return [jaro, jaccard, metric, uniqueNumberCount, numberMatchRate, matchScore, normalizedMatchRate] 

    def string_filter(self, s1, s2):
        '''Filter string with useless characters'''
        s1 = s1.lower()
        s2 = s2.lower()

        return s1.translate(table), s2.translate(table)
    

        



