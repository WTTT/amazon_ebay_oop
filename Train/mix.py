import pandas as pd
import numpy as np
import random
from itertools import permutations

def mix(path, pro_1, pro_2, l):
    data = np.array(pd.read_csv(path, sep=",", encoding='utf-8'))
    
    products_1 = data[:, pro_1] 
    products_2 = data[:, pro_2]
    labels = data[:, l]
    
    result = []

    for pos in range(labels.shape[0]):
        ran_list = [pos]
        while ran_list.count(pos) != 0:
            ran_list = random.sample(range(labels.shape[0]), 10)
        for number in ran_list:
            result.append({'product_1' : products_1[pos], 'product_2' : products_2[number], 'label' : 0})
        result.append({'product_1' : products_1[pos], 'product_2' : products_2[pos], 'label' : labels[pos]})
    # perm = list(permutations(result))
    # new_list = perm[int(len(perm) / 2)]
    df = pd.DataFrame(result)
    df = df.sample(frac=1)
    df.to_csv('mixed_data_latest.csv', index=False)

# mix('Data_fix.csv', 0, 1, 6)
CSV_file = []

Data = np.array(pd.read_csv('amazon_ebay.csv', sep=",", names=['products_1', 'products_2', 'macth']))

products_1 = Data[:, 0]
products_2 = Data[:, 1]
matches = np.array(Data[:, 2], dtype=int)

N = 0

for pos in range(products_1.shape[0]):
    if matches[pos] == 1:
        CSV_file.append({'products_1' : products_1[pos], 'products_2' : products_2[pos], 'match' : matches[pos]})
    if matches[pos] == 0 and N < 45:
        N += 1
        CSV_file.append({'products_1' : products_1[pos], 'products_2' : products_2[pos], 'match' : matches[pos]})

Data = np.array(pd.read_csv('Data (1).csv', sep=","))

products_1 = Data[:, 0]
products_2 = Data[:, 1]
matches = np.array(Data[:, 6], dtype=int)


N = 0
for pos in range(products_1.shape[0]):
    if matches[pos] == 1:
        CSV_file.append({'products_1' : products_1[pos], 'products_2' : products_2[pos], 'match' : matches[pos]})
    if matches[pos] == 0 and N < 50:
        N += 1
        CSV_file.append({'products_1' : products_1[pos], 'products_2' : products_2[pos], 'match' : matches[pos]})
    

df = pd.DataFrame(CSV_file)
df.sample(frac=1.0)
df.to_csv('final_mixed_new_4.csv', index=False)




