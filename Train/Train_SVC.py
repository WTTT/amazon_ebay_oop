import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toSubLibpath()))

from Train import TrainBase
from DataPreProcess import train_test_split

from sklearn.metrics import accuracy_score
from sklearn.svm import SVC

class Training_SVC(TrainBase):
    #OVERRIDE
    def train_to_model(self, ratio=0.8, C=1.0, kernel='rbf', degree=3, gamma='scale', coef0=0.0, shrinking=True, probability=False, tol=0.001, 
    cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape='ovr', break_ties=False, random_state=None):

        self.ratio = ratio

        train_data, test_data, train_labels, test_labels = train_test_split(self.vectors, self.labels, int(self.size_of_data * ratio))

        model = SVC(
           kernel=kernel, degree=degree, gamma=gamma, coef0=coef0, shrinking=shrinking, probability=probability, tol=tol, cache_size=cache_size, 
           class_weight=class_weight, verbose=verbose, max_iter=max_iter, decision_function_shape=decision_function_shape, break_ties=break_ties, random_state=random_state
        )

        model.fit(train_data, train_labels)

        self.accuracy = accuracy_score(model.predict(test_data), test_labels)
        self.model = model