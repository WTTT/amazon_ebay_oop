import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toTrainpath()))

from Train_LinearSVC import Training_LinearSVC

data_name = str(input("Enter data file name: "))

training = Training_LinearSVC(data_name)
training.process_data()
training.train_to_model(C=1.0, random_state=1)

print(training.print_acc())

save = input('Save model? Y/N ')
if save == 'Y':
    model = input('Enter where to save: ')
    print(training.print_and_save(model))