import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toTrainpath()))

from Train_MLPClassifier import Training_MLPClassifier

data_name = str(input("Enter data file name: "))

training = Training_MLPClassifier(data_name)
training.process_data()
training.train_to_model(activation='relu', alpha=0.0001, batch_size='auto', beta_1=0.9,
       beta_2=0.999, early_stopping=False, epsilon=1e-08,
       hidden_layer_sizes=(20, 20, 20), learning_rate='constant',
       learning_rate_init=0.001, max_iter=10000, momentum=0.9,
       nesterovs_momentum=True, power_t=0.5, random_state=None,
       shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
       verbose=False, warm_start=False)

print(training.print_acc())

save = input('Save model? Y/N ')
if save == 'Y':
    model = input('Enter where to save: ')
    print(training.print_and_save(model))